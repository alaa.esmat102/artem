# Drones application

Demo project for communication with delivery drones

## Using technologies
- Java
- Spring Boot
- WebFlux
- R2DBC
- Postgres
- FlyWay

## Dependencies
- Required docker (for integration tests)
- Required docker-compose(or postgres on port 5432)

## Usage

### Database
 - run: `docker-compose up -d`

### Application
- build: `./gradlew clean assemble`
- tests: `./gradlew clean test`
- run: `./gradlew bootRun`


## Contacts
Author: Eremin Artem

Email: 1000001rtem@gmail.com