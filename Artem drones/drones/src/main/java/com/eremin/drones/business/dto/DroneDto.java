package com.eremin.drones.business.dto;

import com.eremin.drones.output.storage.model.Drone;
import com.eremin.drones.output.storage.model.State;
import java.util.UUID;

public record DroneDto(
    UUID serialNumber,
    Long modelId,
    Integer weightLimit,
    Double batteryCapacity,
    State state
) {

    public DroneDto(Drone drone) {
        this(drone.getSerialNumber(), drone.getModelId(), drone.getWeightLimit(), drone.getBatteryCapacity(), drone.getState());
    }
}
