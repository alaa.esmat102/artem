package com.eremin.drones.business.dto;

import com.eremin.drones.output.storage.model.DeliveryStatus;
import com.eremin.drones.output.storage.model.Medication;
import java.util.UUID;

public record MedicationDto(UUID id, String name, Integer weight, String code, Long imageId,
                            DeliveryStatus deliveryStatus, UUID droneId) {

    public MedicationDto(Medication entity) {
        this(entity.getId(), entity.getName(), entity.getWeight(), entity.getCode(), entity.getImageId(), entity.getDeliveryStatus(), entity.getDroneSerialNumber());
    }
}
