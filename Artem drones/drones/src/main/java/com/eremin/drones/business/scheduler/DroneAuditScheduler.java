package com.eremin.drones.business.scheduler;

import com.eremin.drones.business.service.AuditService;
import com.eremin.drones.output.storage.model.Drone;
import com.eremin.drones.output.storage.repository.DroneRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import reactor.core.scheduler.Schedulers;

@Component
@RequiredArgsConstructor
public class DroneAuditScheduler {

    private final DroneRepository droneRepository;
    private final List<AuditService<Drone>> auditServices;

    @Scheduled(cron = "${service.config.drones-audit-time}")
    @SchedulerLock(name = "dronesAuditTask", lockAtMostFor = "5m")
    public void auditDrones() {
        droneRepository.findAll()
            .collectList()
            .map(drones -> {
                auditServices.forEach(it -> it.logAll(drones));
                return drones;
            })
            .subscribeOn(Schedulers.boundedElastic())
            .subscribe();
    }
}
