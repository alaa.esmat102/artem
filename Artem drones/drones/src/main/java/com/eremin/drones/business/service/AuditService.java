package com.eremin.drones.business.service;

import java.util.List;

/**
 * Service for audit logging task
 *
 * @param <T> type of entity to audit
 */
public interface AuditService<T> {

    /**
     * Create log record
     *
     * @param t entity for audit logging
     */
    void log(T t);

    /**
     * Create log record for butch of entities
     *
     * @param t list of entities
     */
    void logAll(List<T> t);
}
