package com.eremin.drones.business.service;

import com.eremin.drones.business.dto.DroneDto;
import com.eremin.drones.input.dto.request.LoadDroneRequest;
import com.eremin.drones.input.dto.request.RegisterDroneRequest;
import java.util.UUID;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service for working with drones
 */
public interface DroneService {

    /**
     * Register a new drone
     *
     * @param request information about new drone
     * @return {@link Mono} new drone id
     * @throws com.eremin.drones.util.error.DronesAppException as {@link reactor.core.publisher.MonoError} If model doesn`t exist
     */
    Mono<UUID> registerDrone(RegisterDroneRequest request);

    /**
     * Load a drone with medications
     *
     * @param request request for loading drone
     * @return {@link Mono} loaded drone id
     * @throws com.eremin.drones.util.error.DronesAppException as {@link reactor.core.publisher.MonoError} <br>
     * - If drone doesn`t exist <br>
     * - Drone has low battery <br>
     * - Drone busy <br>
     * - Drone has overload
     */
    Mono<UUID> loadDrone(LoadDroneRequest request);

    /**
     * Find drones with battery capacity more than 25% and state IDLE
     *
     * @return {@link Flux} list of available drones
     */
    Flux<DroneDto> findAvailableDrones();

    /**
     * Information about battery capacity for specific drone
     *
     * @param serialNumber drone serial number
     * @return {@link Mono} battery level value
     * @throws com.eremin.drones.util.error.DronesAppException as {@link reactor.core.publisher.MonoError} If drone doesn`t exist
     */
    Mono<Double> checkBatteryLevel(UUID serialNumber);
}
