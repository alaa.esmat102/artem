package com.eremin.drones.business.service;

import com.eremin.drones.business.dto.MedicationDto;
import com.eremin.drones.input.dto.request.MedicationInfo;
import java.util.List;
import java.util.UUID;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service for working with medications
 */
public interface MedicationService {

    /**
     * Save butch of medications
     *
     * @param droneSerialNumber serial number of drone
     * @param medicationInfos   list of information about medications to save
     * @return {@link Flux} list of saved medications
     */
    Flux<MedicationDto> saveButch(UUID droneSerialNumber, List<MedicationInfo> medicationInfos);

    /**
     * Find all medications by drone id
     *
     * @param droneSerialNumber drone id
     * @return {@link Mono} list of loaded medications
     */
    Flux<MedicationDto> findAllByDroneId(UUID droneSerialNumber);
}
