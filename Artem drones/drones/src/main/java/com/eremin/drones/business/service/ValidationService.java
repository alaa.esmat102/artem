package com.eremin.drones.business.service;

import reactor.core.publisher.Mono;

public interface ValidationService {

    /**
     * Validate object
     *
     * @param t for validation
     * @return Mono of object or com.eremin.drones.util.error.DronesAppException as {@link reactor.core.publisher.MonoError}
     * if validation failed
     */
    <T> Mono<T> validate(T t);
}
