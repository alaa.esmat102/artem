package com.eremin.drones.business.service.impl;

import com.eremin.drones.business.service.AuditService;
import com.eremin.drones.output.storage.model.Drone;
import com.eremin.drones.output.storage.model.DroneBatteryLevelHistory;
import com.eremin.drones.output.storage.repository.DroneBatteryLevelHistoryRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DroneBatteryLevelAuditService implements AuditService<Drone> {

    //TODO: Send to another service by kafka for example
    private final DroneBatteryLevelHistoryRepository repository;

    @Override
    public void log(Drone drone) {
        repository.save(createLog(drone)).subscribe();
    }

    @Override
    public void logAll(List<Drone> drones) {
        repository.saveAll(
                drones.stream().map(this::createLog).toList()
            )
            .subscribe();
    }

    private DroneBatteryLevelHistory createLog(Drone drone) {
        return new DroneBatteryLevelHistory()
            .setDroneSerialNumber(drone.getSerialNumber())
            .setBatteryLevel(drone.getBatteryCapacity())
            .setIsNewLog(true);
    }
}
