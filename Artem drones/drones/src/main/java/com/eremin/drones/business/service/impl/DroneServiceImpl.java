package com.eremin.drones.business.service.impl;

import com.eremin.drones.business.dto.DroneDto;
import com.eremin.drones.business.service.DroneService;
import com.eremin.drones.business.service.MedicationService;
import com.eremin.drones.config.DronesProperties;
import com.eremin.drones.input.dto.request.LoadDroneRequest;
import com.eremin.drones.input.dto.request.MedicationInfo;
import com.eremin.drones.input.dto.request.RegisterDroneRequest;
import com.eremin.drones.output.storage.model.Drone;
import com.eremin.drones.output.storage.model.DroneModel;
import com.eremin.drones.output.storage.model.State;
import com.eremin.drones.output.storage.repository.DroneModelRepository;
import com.eremin.drones.output.storage.repository.DroneRepository;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.reactive.TransactionalOperator;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import static com.eremin.drones.util.error.Errors.DRONE_IS_BUSY;
import static com.eremin.drones.util.error.Errors.DRONE_NOT_FOUND;
import static com.eremin.drones.util.error.Errors.LOW_BATTERY;
import static com.eremin.drones.util.error.Errors.MODEL_NOT_FOUND;
import static com.eremin.drones.util.error.Errors.OVERWEIGHT;

@Service
@RequiredArgsConstructor
public class DroneServiceImpl implements DroneService {

    private final DroneRepository droneRepository;
    private final DroneModelRepository modelRepository;
    private final MedicationService medicationService;

    private final TransactionalOperator transactionalOperator;
    private final DronesProperties dronesProperties;

    @Override
    public Mono<UUID> registerDrone(RegisterDroneRequest request) {
        return checkModelExist(request.modelId())
            .flatMap(it ->
                droneRepository
                    .save(
                        new Drone()
                            .setModelId(request.modelId())
                            .setWeightLimit(request.weightLimit())
                            .setBatteryCapacity(100.0)
                            .setState(State.IDLE)
                            .setIsNewDrone(true)
                    )
            )
            .map(Drone::getSerialNumber)
            .as(transactionalOperator::transactional)
            .subscribeOn(Schedulers.boundedElastic());
    }

    @Override
    public Mono<UUID> loadDrone(LoadDroneRequest request) {
        return droneRepository.findById(request.serialNumber())
            .switchIfEmpty(Mono.error(DRONE_NOT_FOUND.formatMessage(request.serialNumber().toString()).asException()))
            .flatMap(drone ->
                checkDroneAvailability(drone, countWeight(request.medications()))
                    .flatMap(it -> droneRepository.setState(drone.getSerialNumber(), State.LOADING))
                    .flatMap(it -> medicationService.saveButch(drone.getSerialNumber(), request.medications()).collectList())
                    .flatMap(it -> droneRepository.setState(drone.getSerialNumber(), State.LOADED))
                    .map(it -> drone.getSerialNumber()))
            .as(transactionalOperator::transactional)
            .subscribeOn(Schedulers.boundedElastic());
    }


    @Override
    public Flux<DroneDto> findAvailableDrones() {
        return droneRepository.findAvailable(dronesProperties.getMinBatteryCapacity())
            .map(DroneDto::new)
            .as(transactionalOperator::transactional)
            .subscribeOn(Schedulers.boundedElastic());
    }

    @Override
    public Mono<Double> checkBatteryLevel(UUID serialNumber) {
        return droneRepository
            .getBatteryLevel(serialNumber)
            .switchIfEmpty(Mono.error(DRONE_NOT_FOUND.formatMessage(serialNumber.toString()).asException()))
            .as(transactionalOperator::transactional)
            .subscribeOn(Schedulers.boundedElastic());
    }

    private Mono<DroneModel> checkModelExist(Long id) {
        return modelRepository.findById(id)
            .switchIfEmpty(Mono.error(MODEL_NOT_FOUND.formatMessage(id.toString()).asException()));
    }

    private Integer countWeight(List<MedicationInfo> medications) {
        return medications.stream().mapToInt(MedicationInfo::weight).sum();
    }

    private Mono<Drone> checkDroneAvailability(Drone drone, Integer medicationWeight) {
        if (drone.getBatteryCapacity() < dronesProperties.getMinBatteryCapacity()) {
            return Mono.error(LOW_BATTERY.formatMessage(drone.getSerialNumber().toString()).asException());
        } else if (drone.getState() != State.IDLE) {
            return Mono.error(DRONE_IS_BUSY.formatMessage(drone.getSerialNumber().toString()).asException());
        } else if (medicationWeight > drone.getWeightLimit()) {
            return Mono.error(OVERWEIGHT.formatMessage(drone.getSerialNumber().toString()).asException());
        } else {
            return Mono.just(drone);
        }
    }
}
