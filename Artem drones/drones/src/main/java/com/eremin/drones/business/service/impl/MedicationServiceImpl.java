package com.eremin.drones.business.service.impl;

import com.eremin.drones.business.dto.MedicationDto;
import com.eremin.drones.business.service.MedicationService;
import com.eremin.drones.input.dto.request.MedicationInfo;
import com.eremin.drones.output.storage.model.DeliveryStatus;
import com.eremin.drones.output.storage.model.Image;
import com.eremin.drones.output.storage.model.Medication;
import com.eremin.drones.output.storage.repository.ImageRepository;
import com.eremin.drones.output.storage.repository.MedicationRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.reactive.TransactionalOperator;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

@Service
@RequiredArgsConstructor
public class MedicationServiceImpl implements MedicationService {

    private final MedicationRepository medicationRepository;
    private final ImageRepository imageRepository;

    private final TransactionalOperator transactionalOperator;

    @Override
    public Flux<MedicationDto> saveButch(UUID droneSerialNumber, List<MedicationInfo> medicationInfos) {
        List<Image> images = new ArrayList<>();
        List<Medication> medications = medicationInfos
            .stream()
            .map(it -> {
                var image = new Image().setImage(it.image());
                images.add(image);
                return createEntity(it, droneSerialNumber, image.getId());
            })
            .toList();
        return imageRepository.saveAll(images)
            .collectList()
            .flatMapMany(it -> medicationRepository.saveAll(medications))
            .map(MedicationDto::new)
            .as(transactionalOperator::transactional)
            .subscribeOn(Schedulers.boundedElastic());
    }

    @Override
    public Flux<MedicationDto> findAllByDroneId(UUID droneSerialNumber) {
        return medicationRepository.findByDroneSerialNumber(droneSerialNumber)
            .map(MedicationDto::new)
            .as(transactionalOperator::transactional)
            .subscribeOn(Schedulers.boundedElastic());
    }

    private Medication createEntity(MedicationInfo medicationInfo, UUID droneSerialNumber, Long imageId) {
        return new Medication()
            .setName(medicationInfo.name())
            .setWeight(medicationInfo.weight())
            .setCode(medicationInfo.code())
            .setImageId(imageId)
            .setDroneSerialNumber(droneSerialNumber)
            .setDeliveryStatus(DeliveryStatus.IN_PROGRESS)
            .setIsNewMedication(true);
    }
}
