package com.eremin.drones.business.service.impl;

import com.eremin.drones.business.service.ValidationService;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.Validator;
import reactor.core.publisher.Mono;

import static com.eremin.drones.util.error.Errors.BAD_REQUEST;

@Service
@RequiredArgsConstructor
public class ValidationServiceImpl implements ValidationService {

    private final List<Validator> validators;

    @Override
    public <T> Mono<T> validate(T t) {
        var errors = new BeanPropertyBindingResult(t, t.getClass().getName());
        validators.forEach(validator -> {
            if (validator.supports(t.getClass())) {
                validator.validate(t, errors);
            }
        });
        if (errors.hasErrors()) {
            var fieldsWithError = errors.getFieldErrors()
                .stream()
                .map(FieldError::getField)
                .collect(Collectors.joining(", "));
            var error = BAD_REQUEST.formatMessage(fieldsWithError).asException();
            return Mono.error(error);
        } else {
            return Mono.just(t);
        }
    }
}
