package com.eremin.drones.business.validator;

import com.eremin.drones.config.MedicationsProperties;
import com.eremin.drones.input.dto.request.LoadDroneRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
@RequiredArgsConstructor
public class LoadDroneRequestValidator implements Validator {

    private final MedicationsProperties medicationsProperties;

    @Override
    public boolean supports(final Class<?> clazz) {
        return LoadDroneRequest.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(final Object target, final Errors errors) {
        var request = (LoadDroneRequest) target;
        var medications = request.medications();
        var restrictions = medicationsProperties.getRestrictions();

        ValidationUtils.rejectIfEmpty(
            errors,
            "serialNumber",
            "serialNumber.empty",
            "Drones serial number can`t be empty"
        );
        ValidationUtils.rejectIfEmpty(
            errors,
            "medications",
            "medications.empty",
            "Medications list can`t be null"
        );

        if (medications.isEmpty()) {
            errors.rejectValue(
                "medications",
                "medications.empty",
                "Medications list can`t be empty"
            );
        }

        for (int i = 0; i < medications.size(); i++) {
            errors.pushNestedPath(String.format("medications[%d]", i));
            var medication = medications.get(i);
            if (!medication.name().matches(restrictions.getMedicationNamePattern())) {
                errors.rejectValue(
                    "name",
                    "medications.name.error",
                    "Medication name allowed only letters, numbers, ‘-‘, ‘_’"
                );
            }
            if (!medication.code().matches(restrictions.getMedicationCodePattern())) {
                errors.rejectValue(
                    "code",
                    "medications.code.error",
                    "Medication code allowed only upper case letters, underscore and numbers"
                );
            }
            errors.popNestedPath();
        }
    }
}
