package com.eremin.drones.business.validator;

import com.eremin.drones.config.DronesProperties;
import com.eremin.drones.input.dto.request.RegisterDroneRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
@RequiredArgsConstructor
public class RegisterDroneRequestValidator implements Validator {

    private final DronesProperties properties;

    @Override
    public boolean supports(final Class<?> clazz) {
        return RegisterDroneRequest.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(final Object target, final Errors errors) {
        var request = (RegisterDroneRequest) target;
        var maxWeightCapacity = properties.getRestrictions().getMaxWeightCapacity();

        ValidationUtils.rejectIfEmpty(
            errors,
            "modelId",
            "modelId.empty",
            "Model id can`t be empty"
        );
        ValidationUtils.rejectIfEmpty(
            errors,
            "weightLimit",
            "weightLimit.empty",
            "Weight Limit can`t be empty"
        );

        if (request.weightLimit() > maxWeightCapacity) {
            errors.rejectValue(
                "weightLimit",
                "weightLimit.error",
                String.format("Weight limit can`t be more than %d", maxWeightCapacity)
            );
        }
    }
}
