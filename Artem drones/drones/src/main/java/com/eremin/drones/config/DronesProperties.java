package com.eremin.drones.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@NoArgsConstructor
@AllArgsConstructor
@ConfigurationProperties("service.drones")
public class DronesProperties {
    private int minBatteryCapacity;
    private Restrictions restrictions;

    @Data
    public static class Restrictions {
        private int maxWeightCapacity;
    }
}
