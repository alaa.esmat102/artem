package com.eremin.drones.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@NoArgsConstructor
@AllArgsConstructor
@ConfigurationProperties("service.medications")
public class MedicationsProperties {

    private Restrictions restrictions;

    @Data
    public static class Restrictions {
        private String medicationNamePattern;
        private String medicationCodePattern;
    }
}
