package com.eremin.drones.input.dto.request;

import java.util.List;
import java.util.UUID;

public record LoadDroneRequest(UUID serialNumber, List<MedicationInfo> medications) {
}
