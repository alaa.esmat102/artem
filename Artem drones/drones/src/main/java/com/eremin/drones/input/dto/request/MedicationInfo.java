package com.eremin.drones.input.dto.request;

public record MedicationInfo(
    String name,
    Integer weight,
    String code,
    byte[] image
) {
}
