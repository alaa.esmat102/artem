package com.eremin.drones.input.dto.request;

public record RegisterDroneRequest(Long modelId, Integer weightLimit) {
}
