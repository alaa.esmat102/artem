package com.eremin.drones.input.dto.response;

import java.time.LocalDateTime;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public abstract class DronesAppResponse {

    private LocalDateTime timestamp = LocalDateTime.now();
}
