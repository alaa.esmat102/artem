package com.eremin.drones.input.dto.response;

import lombok.Getter;

@Getter
public class DronesAppSuccessResponse extends DronesAppResponse {

    private final Boolean success;

    public DronesAppSuccessResponse() {
        this.success = true;
    }

}
