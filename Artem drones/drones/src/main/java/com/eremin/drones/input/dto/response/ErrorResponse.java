package com.eremin.drones.input.dto.response;

public record ErrorResponse(String code, String message, String displayMessage) {
}
