package com.eremin.drones.input.rest;

import com.eremin.drones.business.service.DroneService;
import com.eremin.drones.business.service.ValidationService;
import com.eremin.drones.input.dto.request.LoadDroneRequest;
import com.eremin.drones.input.dto.request.RegisterDroneRequest;
import com.eremin.drones.input.dto.response.DronesAppResponse;
import com.eremin.drones.util.transformer.DronesAppResponseTransformer;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/drone")
public class DroneController {

    private final DroneService droneService;
    private final ValidationService validationService;

    @PostMapping
    public Mono<ResponseEntity<DronesAppResponse>> registerDrone(@RequestBody RegisterDroneRequest request) {
        return validationService.validate(request)
            .flatMap(droneService::registerDrone)
            .transform(DronesAppResponseTransformer.transformResponse());
    }

    @PutMapping("/load")
    Mono<ResponseEntity<DronesAppResponse>> loadDrone(@RequestBody LoadDroneRequest request) {
        return validationService.validate(request)
            .flatMap(droneService::loadDrone)
            .transform(DronesAppResponseTransformer.transformResponse());
    }

    @GetMapping("/available")
    Mono<ResponseEntity<DronesAppResponse>> findAllAvailableDrones() {
        return droneService
            .findAvailableDrones()
            .collectList()
            .transform(DronesAppResponseTransformer.transformResponse());
    }

    @GetMapping("/check-battery")
    Mono<ResponseEntity<DronesAppResponse>> checkBattery(@RequestParam("serialNumber") UUID serialNumber) {
        return droneService
            .checkBatteryLevel(serialNumber)
            .transform(DronesAppResponseTransformer.transformResponse());
    }
}
