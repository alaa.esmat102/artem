package com.eremin.drones.input.rest;

import com.eremin.drones.business.service.MedicationService;
import com.eremin.drones.input.dto.response.DronesAppResponse;
import com.eremin.drones.util.transformer.DronesAppResponseTransformer;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/medications")
public class MedicationController {

    public final MedicationService medicationService;

    @GetMapping("/loaded")
    public Mono<ResponseEntity<DronesAppResponse>> findLoadedMedicationsByDroneId(@RequestParam("droneId") UUID droneId) {
        return medicationService
            .findAllByDroneId(droneId)
            .collectList()
            .transform(DronesAppResponseTransformer.transformResponse());
    }
}
