package com.eremin.drones.output.storage.model;

public enum DeliveryStatus {
    IN_PROGRESS, DELIVERED
}
