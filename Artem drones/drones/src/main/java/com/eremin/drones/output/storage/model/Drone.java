package com.eremin.drones.output.storage.model;

import java.util.UUID;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

@Data
@NoArgsConstructor
@Table(name = "drones")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class Drone extends AbstractTable implements Persistable<UUID> {

    @Id
    private UUID serialNumber = UUID.randomUUID();

    private Long modelId;

    private Integer weightLimit;

    private Double batteryCapacity;

    private State state;

    @Transient
    private Boolean isNewDrone;

    @Override
    public UUID getId() {
        return serialNumber;
    }

    @Override
    public boolean isNew() {
        return isNewDrone == null ? Boolean.FALSE : isNewDrone;
    }
}
