package com.eremin.drones.output.storage.model;

import java.time.LocalDateTime;
import java.util.UUID;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Table(name = "drone_battery_level_history")
public class DroneBatteryLevelHistory implements Persistable<UUID> {

    @Id
    private UUID id = UUID.randomUUID();

    private UUID droneSerialNumber;

    private Double batteryLevel;

    private LocalDateTime timestamp = LocalDateTime.now();

    @Transient
    private Boolean isNewLog;

    @Override
    public boolean isNew() {
        return isNewLog == null ? Boolean.FALSE : isNewLog;
    }
}
