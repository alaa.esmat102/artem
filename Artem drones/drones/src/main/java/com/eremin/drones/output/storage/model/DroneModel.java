package com.eremin.drones.output.storage.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Data
@NoArgsConstructor
@Table(name = "models")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class DroneModel extends AbstractTable {

    @Id
    private Long id;

    private String name;
}
