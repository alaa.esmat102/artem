package com.eremin.drones.output.storage.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Data
@NoArgsConstructor
@Table(name = "images")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class Image extends AbstractTable {

    @Id
    private Long id;

    //TODO: I think better store images in somewhere else(blob store for example)
    private byte[] image;
}
