package com.eremin.drones.output.storage.model;


import java.util.UUID;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

@Data
@NoArgsConstructor
@Table(name = "medications")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class Medication extends AbstractTable implements Persistable<UUID> {

    @Id
    private UUID id = UUID.randomUUID();

    private String name;

    private Integer weight;

    private String code;

    private Long imageId;

    private UUID droneSerialNumber;

    private DeliveryStatus deliveryStatus;

    @Transient
    private Boolean isNewMedication;

    @Override
    public boolean isNew() {
        return isNewMedication == null ? Boolean.FALSE : isNewMedication;
    }
}
