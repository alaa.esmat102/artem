package com.eremin.drones.output.storage.model;

public enum State {
    IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING
}
