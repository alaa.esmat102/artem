package com.eremin.drones.output.storage.repository;

import com.eremin.drones.output.storage.model.DroneBatteryLevelHistory;
import java.util.UUID;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface DroneBatteryLevelHistoryRepository extends ReactiveCrudRepository<DroneBatteryLevelHistory, UUID> {
}
