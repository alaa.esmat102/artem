package com.eremin.drones.output.storage.repository;

import com.eremin.drones.output.storage.model.DroneModel;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface DroneModelRepository extends ReactiveCrudRepository<DroneModel, Long> {
}
