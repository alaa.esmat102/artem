package com.eremin.drones.output.storage.repository;

import com.eremin.drones.output.storage.model.Drone;
import com.eremin.drones.output.storage.model.State;
import java.util.UUID;
import org.springframework.data.r2dbc.repository.Modifying;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface DroneRepository extends ReactiveCrudRepository<Drone, UUID> {

    @Query("SELECT * FROM drones WHERE state = 'IDLE' AND battery_capacity > :minCapacity")
    Flux<Drone> findAvailable(@Param("minCapacity") Integer minCapacity);

    @Query("SELECT battery_capacity FROM drones WHERE serial_number = :serialNumber")
    Mono<Double> getBatteryLevel(@Param("serialNumber") UUID serialNumber);

    @Modifying
    @Query("UPDATE drones SET state = :state WHERE serial_number = :serialNumber")
    Mono<Integer> setState(@Param("serialNumber") UUID serialNumber, @Param("state") State state);
}
