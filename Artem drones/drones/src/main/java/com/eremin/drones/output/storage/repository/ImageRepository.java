package com.eremin.drones.output.storage.repository;

import com.eremin.drones.output.storage.model.Image;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface ImageRepository extends ReactiveCrudRepository<Image, Long> {
}
