package com.eremin.drones.output.storage.repository;


import com.eremin.drones.output.storage.model.Medication;
import java.util.UUID;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

public interface MedicationRepository extends ReactiveCrudRepository<Medication, UUID> {

    @Query("SELECT * FROM medications WHERE drone_serial_number= :droneSerialNumber")
    Flux<Medication> findByDroneSerialNumber(@Param("droneSerialNumber") UUID droneSerialNumber);
}
