package com.eremin.drones.util.error;

import lombok.Getter;

@Getter
public class DronesAppException extends RuntimeException {

    private final String code;
    private final String message;
    private final String displayMessage;

    public DronesAppException(final Error error) {
        super(error.getMessage());
        this.code = error.getCode();
        this.message = error.getMessage();
        this.displayMessage = error.getDisplayMessage();
    }
}
