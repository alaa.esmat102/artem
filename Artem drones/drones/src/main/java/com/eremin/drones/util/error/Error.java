package com.eremin.drones.util.error;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Error abstraction
 */
public interface Error {
    String getCode();

    String getMessage();

    String getDisplayMessage();

    /**
     * Convert Error to exception
     *
     * @return {@link DronesAppException}
     */
    default DronesAppException asException() {
        return new DronesAppException(this);
    }

    /**
     * Format error message
     */
    default Error formatMessage(String... values) {
        return new FormattedError(getCode(), String.format(getMessage(), values), getDisplayMessage());
    }

    default Error formatDisplayMessage(String... values) {
        return new FormattedError(getCode(), getMessage(), String.format(getDisplayMessage(), values));
    }

    default Error formatBoth(String... values) {
        return new FormattedError(getCode(), String.format(getMessage(), values), String.format(getDisplayMessage(), values));
    }
}

@Getter
@AllArgsConstructor
class FormattedError implements Error {
    private final String code;
    private final String message;
    private final String displayMessage;
}
