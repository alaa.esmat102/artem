package com.eremin.drones.util.error;

public enum Errors implements Error {
    DRONE_NOT_FOUND("Drone with serial number %s not found", "Drone not found"),
    MODEL_NOT_FOUND("Model with serial number %s not found", "Model not found"),
    LOW_BATTERY("Drone with serial number %s has low battery", "Drone has low battery try another one"),
    DRONE_IS_BUSY("Drone with serial number %s is busy", "Drone is busy try another one"),
    OVERWEIGHT("Drone with serial number %s has overweight", "Drone has overweight try another one"),
    BAD_REQUEST("Unacceptable value for [%s]", "Something went wrong"),
    CRITICAL_ERROR("%s", "Something went wrong"),
    ;

    /**
     * Error code
     */
    private final String code;
    /**
     * Technical message
     */
    private final String message;
    /**
     * Message for user
     */
    private final String displayMessage;

    Errors(final String message, final String displayMessage) {
        this.code = name();
        this.message = message;
        this.displayMessage = displayMessage;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    @Override
    public String getDisplayMessage() {
        return this.displayMessage;
    }
}
