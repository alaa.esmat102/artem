package com.eremin.drones.util.transformer;

import com.eremin.drones.input.dto.response.DronesAppDataResponse;
import com.eremin.drones.input.dto.response.DronesAppErrorResponse;
import com.eremin.drones.input.dto.response.DronesAppResponse;
import com.eremin.drones.input.dto.response.ErrorResponse;
import com.eremin.drones.util.error.DronesAppException;
import com.eremin.drones.util.error.Errors;
import java.util.function.Function;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import reactor.core.publisher.Mono;

/**
 * Api response transformation
 */
@Log4j2
public class DronesAppResponseTransformer {

    /**
     * Transforming a response to standard service response and manage response HTTP codes
     */
    public static Function<Mono<?>, Mono<ResponseEntity<DronesAppResponse>>> transformResponse() {
        return (response) ->
            response
                .flatMap(it ->
                    Mono.just(new ResponseEntity<DronesAppResponse>(
                        new DronesAppDataResponse<>(it),
                        HttpStatus.OK
                    )))
                .onErrorResume(err -> {
                    if (err instanceof DronesAppException error) {
                        log.error(error);
                        return Mono.just(new ResponseEntity<>(
                            new DronesAppErrorResponse(buildErrorResponse(error)),
                            getResponseCode(error.getCode())
                        ));
                    } else {
                        log.error(err);
                        var error = Errors.CRITICAL_ERROR.formatMessage(err.getMessage()).asException();
                        return Mono.just(new ResponseEntity<>(
                            new DronesAppErrorResponse(buildErrorResponse(error)),
                            HttpStatus.INTERNAL_SERVER_ERROR
                        ));
                    }
                });
    }

    private static ErrorResponse buildErrorResponse(DronesAppException exception) {
        return new ErrorResponse(exception.getCode(), exception.getMessage(), exception.getDisplayMessage());
    }

    private static HttpStatus getResponseCode(String errorCode) {
        var error = Errors.valueOf(errorCode);
        return switch (error) {
            case DRONE_NOT_FOUND, MODEL_NOT_FOUND -> HttpStatus.NOT_FOUND;
            case BAD_REQUEST -> HttpStatus.BAD_REQUEST;
            case LOW_BATTERY, DRONE_IS_BUSY, OVERWEIGHT, CRITICAL_ERROR -> HttpStatus.INTERNAL_SERVER_ERROR;
        };
    }
}
