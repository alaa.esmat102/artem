CREATE TABLE drones
(
    serial_number    UUID PRIMARY KEY,
    model_id         BIGINT,
    weight_limit     INT,
    battery_capacity DOUBLE PRECISION,
    state            VARCHAR(255),
    created_at       TIMESTAMP,
    updated_at       TIMESTAMP
);

CREATE TABLE medications
(
    id                  UUID PRIMARY KEY,
    name                VARCHAR(255),
    weight              INT,
    code                VARCHAR(255),
    image_id            BIGINT,
    drone_serial_number UUID,
    delivery_status     VARCHAR(255),
    created_at          TIMESTAMP,
    updated_at          TIMESTAMP
);

CREATE TABLE images
(
    id         BIGSERIAL PRIMARY KEY,
    image      BYTEA,
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);

CREATE TABLE models
(
    id         BIGSERIAL PRIMARY KEY,
    name       VARCHAR(255),
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);

INSERT INTO models (name, created_at, updated_at)
VALUES ('Lightweight', NOW(), NOW()),
       ('Middleweight', NOW(), NOW()),
       ('Cruiserweight', NOW(), NOW()),
       ('Heavyweight', NOW(), NOW());

INSERT INTO drones (serial_number, model_id, weight_limit, battery_capacity, state, created_at, updated_at)
VALUES ('4d1e2b1d-6d57-4f5a-97c1-85905b9d2651', 1, 100, 100.0, 'IDLE', NOW(), NOW()),
       ('5e0b9a6a-98b9-43c8-87e2-97152f80db33', 2, 350, 60.12, 'IDLE', NOW(), NOW()),
       ('f9e3f64d-6e87-4f4a-9f57-d903125de031', 3, 450, 100.0, 'IDLE', NOW(), NOW()),
       ('2d5c4e4f-882c-41d4-9aeb-601f956b50fe', 4, 620, 50.12, 'IDLE', NOW(), NOW()),
       ('c0e8790f-1dd8-4e92-af74-90afcc4ed1ce', 1, 180, 32.86, 'IDLE', NOW(), NOW()),
       ('7b8c09a6-64be-493d-9092-18b5ee782779', 3, 470, 10.54, 'IDLE', NOW(), NOW()),
       ('d85b6dca-2b6d-4ea5-92f5-d6d4d50d174b', 2, 300, 10.78, 'IDLE', NOW(), NOW()),
       ('e9c13b0f-8a29-4422-bd5c-759f6e5a8ff1', 1, 200, 75.0, 'IDLE', NOW(), NOW()),
       ('a2b352ae-328f-4bc0-9e71-25e01e53f2d7', 3, 420, 42.0, 'IDLE', NOW(), NOW()),
       ('d9a78b3c-8e9a-4a72-8c35-001688dd4dbb', 2, 380, 2.23, 'IDLE', NOW(), NOW());

