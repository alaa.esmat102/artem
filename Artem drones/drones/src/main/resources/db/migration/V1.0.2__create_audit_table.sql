CREATE TABLE drone_battery_level_history
(
    id                  UUID PRIMARY KEY,
    drone_serial_number UUID,
    timestamp           TIMESTAMP,
    battery_level       DOUBLE PRECISION
);