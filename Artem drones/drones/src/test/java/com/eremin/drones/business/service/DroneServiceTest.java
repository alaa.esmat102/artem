package com.eremin.drones.business.service;

import com.eremin.drones.business.dto.MedicationDto;
import com.eremin.drones.business.service.impl.DroneServiceImpl;
import com.eremin.drones.config.DronesProperties;
import com.eremin.drones.input.dto.request.LoadDroneRequest;
import com.eremin.drones.input.dto.request.MedicationInfo;
import com.eremin.drones.input.dto.request.RegisterDroneRequest;
import com.eremin.drones.output.storage.model.DeliveryStatus;
import com.eremin.drones.output.storage.model.Drone;
import com.eremin.drones.output.storage.model.DroneModel;
import com.eremin.drones.output.storage.model.State;
import com.eremin.drones.output.storage.repository.DroneModelRepository;
import com.eremin.drones.output.storage.repository.DroneRepository;
import com.eremin.drones.util.error.DronesAppException;
import com.eremin.drones.util.error.Errors;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.ArgumentCaptor;
import org.springframework.transaction.reactive.TransactionalOperator;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static com.eremin.drones.testUtil.TestUtils.defaultDrone;
import static com.eremin.drones.testUtil.TestUtils.mockTransactions;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class DroneServiceTest {

    private DroneRepository droneRepository;
    private DroneModelRepository modelRepository;
    private MedicationService medicationService;

    private DroneService subj;

    @BeforeEach
    void init() {
        droneRepository = mock(DroneRepository.class);
        modelRepository = mock(DroneModelRepository.class);
        medicationService = mock(MedicationService.class);

        TransactionalOperator transactionalOperator = mock(TransactionalOperator.class);
        mockTransactions(transactionalOperator);

        subj = new DroneServiceImpl(droneRepository, modelRepository, medicationService, transactionalOperator, new DronesProperties(25, null));
    }

    @Test
    void shouldSaveNewDrone() {
        when(modelRepository.findById(anyLong()))
            .thenReturn(Mono.just(new DroneModel().setId(42L).setName("testModel")));

        when(droneRepository.save(any())).thenReturn(Mono.just(new Drone()));

        subj.registerDrone(new RegisterDroneRequest(42L, 420))
            .as(StepVerifier::create)
            .expectNextCount(1)
            .verifyComplete();

        verify(modelRepository, times(1)).findById(anyLong());
        verify(droneRepository, times(1)).save(argThat(it -> {
            assertEquals(420, it.getWeightLimit());
            assertEquals(100.0, it.getBatteryCapacity());
            assertEquals(42, it.getModelId());
            assertEquals(State.IDLE, it.getState());
            return true;
        }));
    }

    @Test
    void shouldThrowExceptionWhenModelDoesNotExistWhileRegisterDrone() {
        when(modelRepository.findById(anyLong()))
            .thenReturn(Mono.empty());

        subj.registerDrone(new RegisterDroneRequest(42L, 420))
            .as(StepVerifier::create)
            .expectErrorMatches(err -> {
                assertTrue(err instanceof DronesAppException);
                assertEquals(Errors.MODEL_NOT_FOUND.name(), ((DronesAppException) err).getCode());
                assertEquals(Errors.MODEL_NOT_FOUND.formatMessage(String.valueOf(42)).getMessage(), err.getMessage());
                assertEquals(Errors.MODEL_NOT_FOUND.getDisplayMessage(), ((DronesAppException) err).getDisplayMessage());
                return true;
            })
            .verify();

        verify(modelRepository, times(1)).findById(anyLong());
        verify(droneRepository, never()).save(any());
    }

    @Test
    void shouldLoadDrone() {
        var drone = defaultDrone();
        var medications = List.of(
            new MedicationDto(UUID.randomUUID(), "Medication 1", 20, "Code_1", 1L,
                DeliveryStatus.IN_PROGRESS, drone.getSerialNumber()),
            new MedicationDto(UUID.randomUUID(), "Medication 2", 30, "Code_2", 2L,
                DeliveryStatus.IN_PROGRESS, drone.getSerialNumber()),
            new MedicationDto(UUID.randomUUID(), "Medication 3", 15, "Code_3", 3L,
                DeliveryStatus.IN_PROGRESS, drone.getSerialNumber())
        );
        var dronesInfo = List.of(
            new MedicationInfo("Medication1", 10, "Code1", new byte[]{1, 2, 3}),
            new MedicationInfo("Medication2", 20, "Code2", new byte[]{4, 5, 6}),
            new MedicationInfo("Medication3", 30, "Code3", new byte[]{7, 8, 9})
        );

        ArgumentCaptor<State> stateCapture = ArgumentCaptor.forClass(State.class);
        ArgumentCaptor<UUID> serialNumberCapture = ArgumentCaptor.forClass(UUID.class);
        when(droneRepository.findById(any(UUID.class))).thenReturn(Mono.just(drone));
        when(droneRepository.setState(any(), any())).thenReturn(Mono.just(1));
        when(medicationService.saveButch(any(), any())).thenReturn(Flux.fromIterable(medications));

        subj.loadDrone(new LoadDroneRequest(drone.getSerialNumber(), dronesInfo))
            .as(StepVerifier::create)
            .expectNextCount(1)
            .verifyComplete();

        verify(droneRepository, times(1)).findById(any(UUID.class));
        verify(medicationService, times(1)).saveButch(any(), any());
        verify(droneRepository, times(2)).setState(serialNumberCapture.capture(), stateCapture.capture());

        assertEquals(drone.getSerialNumber(), serialNumberCapture.getValue());
        assertTrue(stateCapture.getAllValues()
            .containsAll(List.of(State.LOADING, State.LOADED))
        );
    }

    @Test
    void shouldThrowExceptionWhenDroneNotFoundWhileDroneLoading() {
        var drone = defaultDrone();
        List<MedicationInfo> medications = List.of();

        when(droneRepository.findById(any(UUID.class))).thenReturn(Mono.empty());

        subj.loadDrone(new LoadDroneRequest(drone.getSerialNumber(), medications))
            .as(StepVerifier::create)
            .expectErrorMatches(err -> {
                assertTrue(err instanceof DronesAppException);
                assertEquals(Errors.DRONE_NOT_FOUND.name(), ((DronesAppException) err).getCode());
                assertEquals(Errors.DRONE_NOT_FOUND.formatMessage(drone.getSerialNumber().toString()).getMessage(), err.getMessage());
                assertEquals(Errors.DRONE_NOT_FOUND.getDisplayMessage(), ((DronesAppException) err).getDisplayMessage());
                return true;
            })
            .verify();

        verify(droneRepository, times(1)).findById(any(UUID.class));
        verify(medicationService, never()).saveButch(any(), any());
        verify(droneRepository, never()).save(any());
    }

    @Test
    void shouldThrowExceptionWhenLowBatteryWhileDroneLoading() {
        var drone = defaultDrone().setBatteryCapacity(10d);
        List<MedicationInfo> medications = List.of();

        when(droneRepository.findById(any(UUID.class))).thenReturn(Mono.just(drone));

        subj.loadDrone(new LoadDroneRequest(drone.getSerialNumber(), medications))
            .as(StepVerifier::create)
            .expectErrorMatches(err -> {
                assertTrue(err instanceof DronesAppException);
                assertEquals(Errors.LOW_BATTERY.name(), ((DronesAppException) err).getCode());
                assertEquals(Errors.LOW_BATTERY.formatMessage(drone.getSerialNumber().toString()).getMessage(), err.getMessage());
                assertEquals(Errors.LOW_BATTERY.getDisplayMessage(), ((DronesAppException) err).getDisplayMessage());
                return true;
            })
            .verify();

        verify(droneRepository, times(1)).findById(any(UUID.class));
        verify(medicationService, never()).saveButch(any(), any());
        verify(droneRepository, never()).save(any());
    }

    @ParameterizedTest
    @EnumSource(value = State.class, mode = EnumSource.Mode.EXCLUDE, names = "IDLE")
    void shouldThrowExceptionWhenDroneBusyWhileDroneLoading(State state) {
        var drone = defaultDrone().setState(state);
        List<MedicationInfo> medications = List.of();

        when(droneRepository.findById(any(UUID.class))).thenReturn(Mono.just(drone));

        subj.loadDrone(new LoadDroneRequest(drone.getSerialNumber(), medications))
            .as(StepVerifier::create)
            .expectErrorMatches(err -> {
                assertTrue(err instanceof DronesAppException);
                assertEquals(Errors.DRONE_IS_BUSY.name(), ((DronesAppException) err).getCode());
                assertEquals(Errors.DRONE_IS_BUSY.formatMessage(drone.getSerialNumber().toString()).getMessage(), err.getMessage());
                assertEquals(Errors.DRONE_IS_BUSY.getDisplayMessage(), ((DronesAppException) err).getDisplayMessage());
                return true;
            })
            .verify();

        verify(droneRepository, times(1)).findById(any(UUID.class));
        verify(medicationService, never()).saveButch(any(), any());
        verify(droneRepository, never()).save(any());
    }

    @Test
    void shouldThrowExceptionWhenOverweightWhileDroneLoading() {
        var drone = defaultDrone().setWeightLimit(50);
        var medications = List.of(
            new MedicationInfo("Medication1", 10, "Code1", new byte[]{1, 2, 3}),
            new MedicationInfo("Medication2", 20, "Code2", new byte[]{4, 5, 6}),
            new MedicationInfo("Medication3", 30, "Code3", new byte[]{7, 8, 9})
        );

        when(droneRepository.findById(any(UUID.class))).thenReturn(Mono.just(drone));

        subj.loadDrone(new LoadDroneRequest(drone.getSerialNumber(), medications))
            .as(StepVerifier::create)
            .expectErrorMatches(err -> {
                assertTrue(err instanceof DronesAppException);
                assertEquals(Errors.OVERWEIGHT.name(), ((DronesAppException) err).getCode());
                assertEquals(Errors.OVERWEIGHT.formatMessage(drone.getSerialNumber().toString()).getMessage(), err.getMessage());
                assertEquals(Errors.OVERWEIGHT.getDisplayMessage(), ((DronesAppException) err).getDisplayMessage());
                return true;
            })
            .verify();

        verify(droneRepository, times(1)).findById(any(UUID.class));
        verify(medicationService, never()).saveButch(any(), any());
        verify(droneRepository, never()).save(any());
    }

    @Test
    void shouldThrowExceptionIfBatteryCapacityDoesNotFound() {
        var serialNumber = UUID.randomUUID();

        when(droneRepository.getBatteryLevel(any(UUID.class))).thenReturn(Mono.empty());

        subj.checkBatteryLevel(serialNumber)
            .as(StepVerifier::create)
            .expectErrorMatches(err -> {
                assertTrue(err instanceof DronesAppException);
                assertEquals(Errors.DRONE_NOT_FOUND.name(), ((DronesAppException) err).getCode());
                assertEquals(Errors.DRONE_NOT_FOUND.formatMessage(serialNumber.toString()).getMessage(), err.getMessage());
                assertEquals(Errors.DRONE_NOT_FOUND.getDisplayMessage(), ((DronesAppException) err).getDisplayMessage());
                return true;
            })
            .verify();

        verify(droneRepository, times(1)).getBatteryLevel(any());
    }
}