package com.eremin.drones.business.service;

import com.eremin.drones.business.service.impl.MedicationServiceImpl;
import com.eremin.drones.input.dto.request.MedicationInfo;
import com.eremin.drones.output.storage.model.DeliveryStatus;
import com.eremin.drones.output.storage.model.Image;
import com.eremin.drones.output.storage.model.Medication;
import com.eremin.drones.output.storage.repository.ImageRepository;
import com.eremin.drones.output.storage.repository.MedicationRepository;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.transaction.reactive.TransactionalOperator;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import static com.eremin.drones.testUtil.TestUtils.mockTransactions;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class MedicationServiceTest {
    private MedicationRepository medicationRepository;
    private ImageRepository imageRepository;

    private MedicationService subj;

    @BeforeEach
    void init() {
        medicationRepository = mock(MedicationRepository.class);
        imageRepository = mock(ImageRepository.class);

        TransactionalOperator transactionalOperator = mock(TransactionalOperator.class);
        mockTransactions(transactionalOperator);

        subj = new MedicationServiceImpl(medicationRepository, imageRepository, transactionalOperator);
    }

    @Test
    void shouldSaveButchOfMedications() {
        var droneSerialNumber = UUID.randomUUID();
        var dronesInfo = List.of(
            new MedicationInfo("Medication1", 10, "Code1", new byte[]{1, 2, 3}),
            new MedicationInfo("Medication2", 20, "Code2", new byte[]{4, 5, 6}),
            new MedicationInfo("Medication3", 30, "Code3", new byte[]{7, 8, 9})
        );
        var medications = List.of(
            new Medication().setId(UUID.randomUUID()).setName("Medication1").setWeight(10).setCode("Code1")
                .setImageId(1L).setDroneSerialNumber(droneSerialNumber).setDeliveryStatus(DeliveryStatus.IN_PROGRESS),
            new Medication().setId(UUID.randomUUID()).setName("Medication2").setWeight(20).setCode("Code2")
                .setImageId(2L).setDroneSerialNumber(droneSerialNumber).setDeliveryStatus(DeliveryStatus.IN_PROGRESS),
            new Medication().setId(UUID.randomUUID()).setName("Medication3").setWeight(30).setCode("Code3")
                .setImageId(3L).setDroneSerialNumber(droneSerialNumber).setDeliveryStatus(DeliveryStatus.IN_PROGRESS)
        );
        var images = List.of(
            new Image().setId(1L).setImage(new byte[]{1, 2, 3}),
            new Image().setId(2L).setImage(new byte[]{4, 5, 6}),
            new Image().setId(3L).setImage(new byte[]{7, 8, 9})
        );

        when(medicationRepository.saveAll(anyList())).thenReturn(Flux.fromIterable(medications));
        when(imageRepository.saveAll(anyList())).thenReturn(Flux.fromIterable(images));

        var imageIds = images.stream().map(Image::getId).toList();
        subj.saveButch(droneSerialNumber, dronesInfo)
            .as(StepVerifier::create)
            .expectNextMatches(it -> {
                assertEquals(droneSerialNumber, it.droneId());
                assertTrue(imageIds.contains(it.imageId()));
                return true;
            })
            .expectNextMatches(it -> {
                assertEquals(droneSerialNumber, it.droneId());
                assertTrue(imageIds.contains(it.imageId()));
                return true;
            })
            .expectNextMatches(it -> {
                assertEquals(droneSerialNumber, it.droneId());
                assertTrue(imageIds.contains(it.imageId()));
                return true;
            })
            .verifyComplete();

        verify(imageRepository, times(1)).saveAll(anyList());
        verify(medicationRepository, times(1)).saveAll(anyList());
    }

}