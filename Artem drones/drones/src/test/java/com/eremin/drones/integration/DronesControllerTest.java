package com.eremin.drones.integration;

import com.eremin.drones.output.storage.repository.DroneRepository;
import com.eremin.drones.testUtil.TestUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;

import static com.eremin.drones.util.error.Errors.BAD_REQUEST;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@AutoConfigureWebTestClient
public class DronesControllerTest extends IntegrationTest {

    @Autowired
    private DroneRepository repository;

    @Autowired
    private WebTestClient client;

    @Test
    void shouldReturnBadRequestWhenDroneWhenTheMaximumLoadCapacityIsExceeded() {
        var beforeSize = repository.findAll().collectList().block();
        var request = TestUtils.getRequestJson("request/register_drone_bad_weight_limit_request.json");
        client.post()
            .uri("/api/v1/drone")
            .contentType(APPLICATION_JSON)
            .body(BodyInserters.fromValue(request))
            .exchange()
            .expectStatus()
            .isBadRequest()
            .expectBody()
            .jsonPath("$.error.code").isEqualTo(BAD_REQUEST.getCode())
            .jsonPath("$.error.message").isEqualTo("Unacceptable value for [weightLimit]")
            .jsonPath("$.error.displayMessage").isEqualTo("Something went wrong");

        assertEquals(beforeSize, repository.findAll().collectList().block());
    }

    @Test
    void shouldReturnBadRequestWhenMedicationHasBadName() {
        var beforeSize = repository.findAll().collectList().block();
        var request = TestUtils.getRequestJson("request/load_drone_bad_medication_name_request.json");
        client.put()
            .uri("/api/v1/drone/load")
            .contentType(APPLICATION_JSON)
            .body(BodyInserters.fromValue(request))
            .exchange()
            .expectStatus()
            .isBadRequest()
            .expectBody()
            .jsonPath("$.error.code").isEqualTo(BAD_REQUEST.getCode())
            .jsonPath("$.error.message").isEqualTo("Unacceptable value for [medications[1].name]")
            .jsonPath("$.error.displayMessage").isEqualTo("Something went wrong");

        assertEquals(beforeSize, repository.findAll().collectList().block());
    }

    @Test
    void shouldReturnBadRequestWhenMedicationHasBadCode() {
        var beforeSize = repository.findAll().collectList().block();
        var request = TestUtils.getRequestJson("request/load_drone_bad_medication_code_request.json");
        client.put()
            .uri("/api/v1/drone/load")
            .contentType(APPLICATION_JSON)
            .body(BodyInserters.fromValue(request))
            .exchange()
            .expectStatus()
            .isBadRequest()
            .expectBody()
            .jsonPath("$.error.code").isEqualTo(BAD_REQUEST.getCode())
            .jsonPath("$.error.message").isEqualTo("Unacceptable value for [medications[1].code]")
            .jsonPath("$.error.displayMessage").isEqualTo("Something went wrong");

        assertEquals(beforeSize, repository.findAll().collectList().block());
    }
}
