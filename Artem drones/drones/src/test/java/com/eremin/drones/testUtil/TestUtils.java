package com.eremin.drones.testUtil;

import com.eremin.drones.output.storage.model.Drone;
import com.eremin.drones.output.storage.model.State;
import java.nio.charset.StandardCharsets;
import org.springframework.core.io.ClassPathResource;
import org.springframework.transaction.reactive.TransactionalOperator;
import org.springframework.util.FileCopyUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class TestUtils {

    public static Drone defaultDrone() {
        return new Drone()
            .setModelId(42L)
            .setWeightLimit(200)
            .setBatteryCapacity(50D)
            .setState(State.IDLE);
    }

    public static void mockTransactions(TransactionalOperator transactionalOperator) {
        when(transactionalOperator.transactional(any(Flux.class))).thenAnswer(it ->
            it.getArguments()[0]
        );
        when(transactionalOperator.transactional(any(Mono.class))).thenAnswer(it ->
            it.getArguments()[0]
        );
    }

    public static String getRequestJson(String path) {
        try {
            ClassPathResource resource = new ClassPathResource(path);
            byte[] bytes = FileCopyUtils.copyToByteArray(resource.getInputStream());
            return new String(bytes, StandardCharsets.UTF_8);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}